class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :contact
      t.references :disease, index: true
      t.string :hospital

      t.timestamps
    end
  end
end
