class CreateFunds < ActiveRecord::Migration
  def change
    create_table :funds do |t|
      t.decimal :amount
      t.references :disease, index: true

      t.timestamps
    end
  end
end
