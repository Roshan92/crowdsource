class Disease < ActiveRecord::Base
  has_many :funds
  has_many :topics
  has_many :doctors
end
