ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

   content title: proc{ I18n.t("active_admin.dashboard") } do
  #   div class: "blank_slate_container", id: "dashboard_default_message" do
  #     span class: "blank_slate" do
  #       span I18n.t("active_admin.dashboard_welcome.welcome")
  #       small I18n.t("active_admin.dashboard_welcome.call_to_action")
  #     end
  #   end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "Recent Topics discussed on Forum" do
          ul do
            Topic.limit(5).map do |topic|
              li link_to(topic.title, admin_topic_path(topic))
            end
          end
        end
      end

      column do
        panel "About Us" do
          para "Welcome to Crowdsourcing. Crowdsourcing is a new way to share information aoout diseases. We’re a home for every disease in the world. You contribution either big or small will greatly contribute to the betterment of the world. Together we fight ! "
        end
      end
    end
  end # content
end
