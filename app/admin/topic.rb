ActiveAdmin.register Topic do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  menu :label => "Forum"

  permit_params :title, :description, :disease_id

  index do
    selectable_column
    column 'Question', :title
    column :description
    column :created_at
    column :updated_at
    actions
  end

  show do |topic|
    attributes_table do
      row :description
    end
    active_admin_comments
  end

end
