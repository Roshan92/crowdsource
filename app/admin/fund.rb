ActiveAdmin.register Fund do

  actions :all, :except => [:destroy, :edit]
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  index do
    selectable_column
    column :disease
    column :amount, :sortable => :amount do |fund|
      div :class => "Amount" do
        number_to_currency fund.amount
      end
    end
    column "Donation date & time", :created_at
    actions
  end

  show do |fund|
    attributes_table do
      row :disease
      row :amount
      render "donate"
    end
  end

  permit_params :amount, :disease_id, :email

  config.comments = false

end
